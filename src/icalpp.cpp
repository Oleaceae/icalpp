#include "icalpp.h"

#include <fstream>
#include <iostream>
#include <ranges>
#include <sstream>

using namespace icalpp;

static std::string readPropertyString(const std::basic_string_view<char> line) {
  const auto property = line.substr(line.find(':') + 1);
  return std::string(property);
}

std::expected<ICal, icalpp::ParseError>
ICal::fromFile(const std::string& path) {
  std::ifstream file(path, std::ios::binary);

  if (!file.is_open()) {
    return ICal();
  }

  std::stringstream buffer;
  buffer << file.rdbuf();
  return ICal::fromString(buffer.str());
}

std::expected<ICal, icalpp::ParseError> ICal::fromString(std::string str) {
  ICal::unfold(str);

  ICal cal{};

  auto lines = std::ranges::views::split(str, ICal::delim);

  if (std::string_view(*lines.begin()).find("BEGIN:VCALENDAR") != 0) {
    return std::unexpected<ParseError>(
        ParseError::missing_icalendar_object_delimiter_start);
  }

  for (const auto line : lines) {
    cal.readLine(std::string_view(line));
  }

  return cal;
}

void ICal::unfold(std::string& str) noexcept {
  std::size_t start{};
  for (start = str.find("\r\n", start); start != std::string::npos;
       start = str.find("\r\n", start)) {
    if (str.size() <= (start + 2)) {
      return;
    }
    if (str[start + 2] == ' ' || str[start + 2] == '\t') {
      str.erase(start, 3);
    } else {
      start += 2;
    }
  }
}
void ICal::readLine(std::basic_string_view<char> line) noexcept {

  properties.readLine(line);
}

void CalendarProperties::readLine(std::basic_string_view<char> line) noexcept {
  if (line.find("VERSION:") == 0) {
    version = readPropertyString(line);
  } else if (line.find("CALSCALE:") == 0) {
    calscale = readPropertyString(line);
  } else if (line.find("METHOD:") == 0) {
    method = readPropertyString(line);
  } else if (line.find("PRODID:") == 0) {
    prodid = readPropertyString(line);
  }
}

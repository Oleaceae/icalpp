#ifndef ICALPP_ICALPP_H
#define ICALPP_ICALPP_H

#include <string>
#include <expected>

namespace icalpp {

enum class ParseError {
  missing_icalendar_object_delimiter_start,
};

class CalendarProperties {
  friend class ICal;
public:
  std::string prodid{};
  std::string version{};
  std::string calscale{"GREGORIAN"};
  std::string method{};
private:
  void readLine(std::basic_string_view<char> line) noexcept;
};

class ICal {
public:
  ICal() = default;

  static std::expected<ICal, icalpp::ParseError> fromFile(const std::string& path);
  static std::expected<ICal, icalpp::ParseError> fromString(std::string str);

  CalendarProperties properties{};
private:
  void readLine(std::basic_string_view<char> line) noexcept;
  static void unfold(std::string& str) noexcept;

  static constexpr std::string_view delim{"\r\n"};
};

} // namespace icalpp

#endif // ICALPP_ICALPP_H

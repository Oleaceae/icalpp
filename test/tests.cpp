#define CATCH_CONFIG_MAIN
#include "extern/catch.hpp"

#include "../src/icalpp.h"

using namespace icalpp;

TEST_CASE("Construction of calendar objects") {
  auto cal = ICal::fromFile("samples/sample.ics");

  REQUIRE(cal.has_value());
}
